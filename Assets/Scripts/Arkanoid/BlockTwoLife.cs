﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTwoLife : BlockArkanoid
{
    protected override void Start(){
        lives=2;
        Colorear();
    }

    protected override void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float r,g,b;
        r = 1.0f;
        g = 1.0f;
        b = 0;

        sp.color = new Color(r,g,b,1f);
    }
}
