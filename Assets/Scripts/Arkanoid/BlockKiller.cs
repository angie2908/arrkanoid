﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockKiller : BlockArkanoid
{
    public GameObject killer;

    public override void TouchBall(){
        GameObject go = Instantiate(killer);
        go.transform.position = transform.position;
        Destroy(gameObject);
    }
}
